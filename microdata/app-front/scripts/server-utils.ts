export function getCurrentServerName() {
    const nuxtApp = useNuxtApp()
    let host = ''

    // Within Nuxt, it's possible to run on server side or client side
    // depending on that, the host has to be retrieved either from window.location in the browser
    // or from the user request from the server
    // For the sake of the demo, we assume the server is running on localhost:8080
    // this piece of code should be shared for all application
    if (process.server) {
        console.log("Running in SSR mode")
        host = nuxtApp.ssrContext.req.headers.host
    } else {
        console.log("Running in client side mode")
        host = window.location.host
    }
    let hostWithoutPort = host.split(':')[0]

    return 'http://' + hostWithoutPort + ":8080";
}
