package com.malt.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/host-settings")
@CrossOrigin(origins = "*")
public class HostSettingsApi {

    @GetMapping
    public HostSetting getHostSettings(HttpServletRequest request) {
        String host = request.getServerName();

        return switch (host) {
            case "localhost" ->
                    new HostSetting("https://localhost", "localhost@malt.com", List.of("https://instagram/localhost", "https://twitter/localhost"));
            case "dev.malt.fr" ->
                    new HostSetting("https://dev.malt.fr", "contact@malt.fr", List.of("https://instagram/fr", "https://twitter/fr"));
            case "dev.malt.es" ->
                    new HostSetting("https://dev.malt.es", "contact@malt.es", List.of("https://instagram/es", "https://twitter/es"));
            default -> new HostSetting("undefined", "undefined", List.of());
        };
    }

}
