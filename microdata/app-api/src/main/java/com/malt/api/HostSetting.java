package com.malt.api;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class HostSetting {

    private String url;
    private String email;
    private List<String> socialNetWorks;

}
