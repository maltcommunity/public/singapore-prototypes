export default defineEventHandler(async (event) => {

    const query = useQuery(event)
    await useStorage().setItem('singapore:migration', query.enabled)

    return "ok " + query.enabled
})
