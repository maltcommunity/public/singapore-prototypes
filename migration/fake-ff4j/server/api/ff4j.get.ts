export default defineEventHandler(async (event) => {

    const data = await useStorage().getItem('singapore:migration')

    return data ? data == true : false
})
