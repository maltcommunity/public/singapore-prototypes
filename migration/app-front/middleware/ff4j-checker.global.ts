import axios from "axios";

export default defineNuxtRouteMiddleware(async (to, from) => {

    const ff4j = await axios.get('/api/ff4j')

    if (!ff4j.data) {
        const cookie = useCookie('ff4j-enabled')
        cookie.value = undefined

        if (process.client) {
            window.location.href = '/'
        }
        if (process.server) {
           useNuxtApp().$router.replace('/')
        }
    }


})
