import axios from "axios";

export default defineNuxtRouteMiddleware(async (to, from) => {

    const ff4j = await axios.get('http://dev.malt.fr/api/ff4j')

    if (ff4j.data) {
        const cookie = useCookie('ff4j-enabled')
        cookie.value = "true"

        if (process.client) {
            window.location.href = '/'
        }
        if (process.server) {
            useNuxtApp().$router.replace('/')
        }
    }


})
