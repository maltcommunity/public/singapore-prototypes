= Singapore

Managing security in a nuxt application with Spring security and JWT



== Context

On this project we want to demonstrate :

- how to proxify back and frontend application behind the same url on port 80 (it's mandatory for cookie management and security)
- how to authenticate users with Spring security using a JWT Token
- how to persist the state (being logged) in the nuxt application
- how to protect portion of a page from the role of the user
- how to guard navigation to a protected page from the role of the user
- how to check the JWT token on every request and extend its life time
- how to protect the content of the page from the role of the user
- how to use CSRF protection
- how to switch identities and enrich the JWT token with this information
- how to impersonate a user and keep this information in the JWT token


== Prerequisites

in the backend application :

   mvn clean install
   mvn spring-boot:run

The backend application will start on port 8080


In the frontend application :

    npm install
    npm run dev

The frontend application will start on port 3000

Launch traefik with command line:

    traefik

(The configuration file is in the root directory, the same folder as this README)

Now it's possible to acces the application on :

    http://localhost

or if your configured your /etc/hosts file :

    http://dev.malt.fr

You have 3 users :

* admin/admin
* moderator/moderator
* user/user

You can try to login, navigate the website, refresh the page (to test the persistence) and logout
