package com.malt.api.security;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Impersonation {

    private boolean impersonated;
    private String impersonatedBy;
}
