package com.malt.api.security.domain;

import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public class UserRepository {

    private static String IDENTITY_1 = UUID.randomUUID().toString();
    private static String IDENTITY_2 = UUID.randomUUID().toString();
    private static String IDENTITY_3 = UUID.randomUUID().toString();
    private static String IDENTITY_4 = UUID.randomUUID().toString();
    private static String IDENTITY_5 = UUID.randomUUID().toString();
    private static String IDENTITY_6 = UUID.randomUUID().toString();

    public Optional<User> findByUsernameOrEmail(String username) {

        if (username.equals("admin")) {
            User user = new User("admin", "admin@malt.com", "$2y$10$cIUK5IpBWA61Rmqsya020emJ01KRtWpgQopIlakWyKuMvX4JT53lq");
            user.setIdentities(Set.of(new Identity(IDENTITY_1), new Identity(IDENTITY_2)));
            user.setRoles(Set.of(RoleType.ROLE_ADMIN));
            return Optional.of(user);
        }
        if (username.equals("moderator")) {
            User user = new User("moderator", "moderator@malt.com", "$2y$10$inR/v.8nBIao8z34Zigqo..RwrklThsPBAvyxydlFePZHOnNpLHhy");
            user.setIdentities(Set.of(new Identity(IDENTITY_3), new Identity(IDENTITY_4)));
            user.setRoles(Set.of(RoleType.ROLE_MODERATOR));
            return Optional.of(user);
        }
        if (username.equals("user")) {
            User user = new User("user", "user@malt.com", "$2y$10$LZQncbnE1/Q2pG571RrdWOW0i36Oinier0B8Xl3uVn/eMU8nWdGn.");
            user.setIdentities(Set.of(new Identity(IDENTITY_5), new Identity(IDENTITY_6)));
            user.setRoles(Set.of(RoleType.ROLE_USER));
            return Optional.of(user);
        }
        else return Optional.empty();
    }
}
