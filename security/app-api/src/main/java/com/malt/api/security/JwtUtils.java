package com.malt.api.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.malt.api.security.payload.response.JwtResponse;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${malt.app.jwtSecret}")
    private String jwtSecret;

    @Value("${malt.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    public Cookie createJwtCookie(JwtResponse jwtResponse) throws JsonProcessingException {

        String tokenInJson = objectMapper.writeValueAsString(jwtResponse);
        Cookie cookie = new Cookie("auth", Base64.getEncoder().encodeToString(tokenInJson.getBytes()));
        cookie.setHttpOnly(true);

        // ! important
        // the cookie should be set to secure in a real environment with https
//    cookie.setSecure(true);

        cookie.setMaxAge(jwtExpirationMs / 1000);
        cookie.setPath("/");

        return cookie;
    }

    public Optional<JwtResponse> readJwtResponseFromCookie(HttpServletRequest request) {
        Cookie cookie = readJwtCookie(request);
        if (cookie == null) {
            return Optional.empty();
        }
        String tokenInJson = new String(Base64.getDecoder().decode(cookie.getValue()));
        try {
            return Optional.of(objectMapper.readValue(tokenInJson, JwtResponse.class));
        } catch (IOException e) {
            logger.error("Cannot read JwtResponse from cookie:", e);
            return Optional.empty();
        }
    }

    private Cookie readJwtCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("auth")) {
                    return cookie;
                }
            }
        }
        return null;
    }

    public Cookie removeJwtCookie() {
        Cookie cookie = new Cookie("auth", null);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        cookie.setMaxAge(0);
        return cookie;
    }

    public String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }

        return null;
    }

    public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder().setSubject((userPrincipal.getUsername())).setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs)).signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
