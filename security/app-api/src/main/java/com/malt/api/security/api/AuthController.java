package com.malt.api.security.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.malt.api.security.Impersonation;
import com.malt.api.security.JwtUtils;
import com.malt.api.security.UserDetailsImpl;
import com.malt.api.security.UserDetailsServiceImpl;
import com.malt.api.security.payload.request.LoginRequest;
import com.malt.api.security.payload.response.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @GetMapping("/isLogged")
    @PreAuthorize("isAuthenticated()")
    /**
     * By design, this endpoint is only accessible to authenticated users.
     * The purpose of the response is only to refresh the token and extend its life time
     */
    public ResponseEntity isLogged(HttpServletResponse response) throws JsonProcessingException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        JwtResponse jwtResponse = createJwtResponse(authentication, userDetails);

        response.addCookie(jwtUtils.createJwtCookie(jwtResponse));

        return ResponseEntity.ok(jwtResponse);
    }


    @PostMapping("/sudosu/{user}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity sudosu(@PathVariable String user, HttpServletResponse response) throws JsonProcessingException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        // the token is validated, and the user is an admin
        // we load the user to impersonate from the database and we change the jwt token to impersonate the user
        // however, we have to keep the information of the current admin to be able to restore the impersonation
        String initialUser = userDetails.getUsername();

        UserDetailsImpl userDetailsFromDb = (UserDetailsImpl) userDetailsService.loadUserByUsername(user);
        userDetailsFromDb.setImpersonation(new Impersonation(true, initialUser));

        UsernamePasswordAuthenticationToken authenticated =
                UsernamePasswordAuthenticationToken.authenticated(userDetailsFromDb, userDetailsFromDb.getPassword(), userDetailsFromDb.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticated);

        JwtResponse jwtResponse = createJwtResponse(authenticated, userDetailsFromDb);

        response.addCookie(jwtUtils.createJwtCookie(jwtResponse));

        return ResponseEntity.ok(jwtResponse);
    }

    @PostMapping("/identity/switch/{identity}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity switchIdentity(@PathVariable String identity, HttpServletResponse response) throws JsonProcessingException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        userDetails.setSelectedIdentity(identity);

        JwtResponse jwtResponse = createJwtResponse(authentication, userDetails);

        response.addCookie(jwtUtils.createJwtCookie(jwtResponse));

        return ResponseEntity.ok(jwtResponse);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletResponse response) throws JsonProcessingException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        JwtResponse jwtResponse = createJwtResponse(authentication, userDetails);

        response.addCookie(jwtUtils.createJwtCookie(jwtResponse));

        return ResponseEntity.ok(jwtResponse);
    }

    @PostMapping("/logout")
    public void logout(HttpServletResponse response) {
        response.addCookie(jwtUtils.removeJwtCookie());
    }


    private JwtResponse createJwtResponse(Authentication authentication, UserDetailsImpl userDetails) {
        String jwt;
        jwt = jwtUtils.generateJwtToken(authentication);

        JwtResponse jwtResponse = new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getRoles(),
                userDetails.getIdentities(),
                userDetails.getSelectedIdentity());

        if (userDetails.getImpersonation() != null) {
            jwtResponse.setImpersonated(userDetails.getImpersonation().isImpersonated());
            jwtResponse.setImpersonatedBy(userDetails.getImpersonation().getImpersonatedBy());
        }

        return jwtResponse;
    }
}
