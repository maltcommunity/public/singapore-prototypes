package com.malt.api.security;

import com.malt.api.security.payload.response.JwtResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthTokenFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        try {
            String jwt = jwtUtils.parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                String username = jwtUtils.getUserNameFromJwtToken(jwt);

                UserDetailsImpl userDetails = (UserDetailsImpl)userDetailsService.loadUserByUsername(username);

                enrichUserDetailsVariablePartWithInfoFromJwtCookie(request, userDetails);

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null,
                        userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (UsernameNotFoundException e) {
            logger.error("{} ", e.getMessage());
        } catch (Exception e) {
            logger.error("Cannot set user authentication:", e);
        }

        filterChain.doFilter(request, response);
    }

    private void enrichUserDetailsVariablePartWithInfoFromJwtCookie(HttpServletRequest request, UserDetailsImpl userDetails) {
        jwtUtils.readJwtResponseFromCookie(request).ifPresent(jwtResponse -> {
            userDetails.setImpersonation(new Impersonation(jwtResponse.isImpersonated(), jwtResponse.getImpersonatedBy()));
            userDetails.setSelectedIdentity(jwtResponse.getSelectedIdentity());
        });
    }

}
