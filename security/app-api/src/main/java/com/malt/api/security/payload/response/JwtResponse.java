package com.malt.api.security.payload.response;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;

	private Set<String> identities;

	private String selectedIdentity;

	private boolean impersonated = false;

	private String impersonatedBy;

	public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles, Set<String> identities, String selectedIdentity) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.identities = identities;
		this.selectedIdentity = selectedIdentity;
	}
}
