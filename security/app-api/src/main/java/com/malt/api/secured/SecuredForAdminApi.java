package com.malt.api.secured;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/secured/admin")
@CrossOrigin(origins = "*")
public class SecuredForAdminApi {

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public String getContentSecuredForAdmin(HttpServletRequest request) {
        return "This content is secured for admin only";
    }

}
