package com.example.securingweb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/legacy/greetings")
public class GreetingController {

    @GetMapping
    public ModelAndView greetings(HttpServletRequest request) {
        System.out.println(request.getSession().getId());
        return new ModelAndView("greetings", "message", "Hello World!");
    }

}
