import {defineStore} from 'pinia'
import axios from "axios";
import {User} from "~/models/User";

const API_URL = 'http://dev.malt.fr/api/auth/';

export const useAuth = defineStore({
    id: 'auth',

    state: () => {
        let user = null as User;
        if (process.server) {
            let auth = useCookie('auth').value;
            if (auth) {
                let buff = new Buffer(auth, 'base64');
                user = JSON.parse(buff.toString('ascii'));
            }
        }
        const initialState = {status: {loggedIn: !!user}, user: user as User};

        return {
            state: initialState
        }
    },

    actions: {
        async login(username: string, password: string) {

            const xsrfCookie = useCookie('XSRF-TOKEN').value;

            let response = await useFetch(API_URL + 'signin', {
                method: 'post',
                body: {
                    username: username,
                    password: password
                },
                headers: {
                    'X-XSRF-TOKEN': xsrfCookie
                }
            });

            this.state.status.loggedIn = true;
            this.state.user = response.data;
        },
        logout(): void {
            useSecuredPostAxios(API_URL + 'logout', {}).then(() => {
                this.state.status.loggedIn = false;
                this.state.user = null;
                useRouter().push('/');
            });
        },
        switchIdentity(identity: string): void {
            useSecuredPostAxios(API_URL + `identity/switch/${identity}`, {}).then((response) => {
                this.state.user = response.data;
            });
        },
        impersonate(user: string): void {
            useSecuredPostAxios(API_URL + `sudosu/${user}`, {}).then((response) => {
                this.state.user = response.data;
            });
        },
        authHeader(): any {
            if (this.state.user) {
                return {Authorization: 'Bearer ' + this.state.user.token};
            }
            return {};
        },
        async isLogged(): Promise<boolean> {
            // Here, we could just check the state in the store. However maybe we don't trust that or
            // we just want to check if the user is still logged in (the token is maybe revoked or expired)
            // So, we just call the API and we may also have an updated version of the user


            // Weird, if I use useFetch here, there is no call. Maybe useFetch cache the response?
            // let response = await useFetch(API_URL + 'isLogged', {method : 'post', headers: this.authHeader()});
            try {
                let response = await useSecuredGetAxios(API_URL + 'isLogged');
                this.state.status.loggedIn = true;
                this.state.user = response.data;

            } catch (e) {
                this.state.status.loggedIn = false;
                this.state.user = null;
            }

            return this.state.status.loggedIn;
        }
    },
});
