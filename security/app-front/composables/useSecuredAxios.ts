import axios from 'axios';
import {useAuth} from "~/store/auth.store";


export const useSecuredPostAxios = (url: string, data, options?) => {
    const store = useAuth();
    const headers = store.authHeader();

    const xsrfCookie = useCookie('XSRF-TOKEN').value;
    const headersWithXSRF = {headers, 'x-xsrf-token': xsrfCookie};
    return axios.post(url, data,{...options, ...headersWithXSRF});
}

export const useSecuredGetAxios = (url: string, options?) => {
    const store = useAuth();
    const headers = store.authHeader();
    return axios.get(url, {...options, headers});
}
