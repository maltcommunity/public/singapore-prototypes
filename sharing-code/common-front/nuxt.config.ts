import { defineNuxtConfig } from 'nuxt/config'

// more information here to include external 3rd party libraries in nuxt : https://dev.to/iggredible/how-to-load-external-script-in-nuxt-app-7a9


// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    modules: ['@pinia/nuxt'],

    app: {
        head: {
            link: [
                {
                    // To enable login/signup through facebook connect
                    rel: 'dns-prefetch',
                    href: 'https://connect.facebook.net',
                },
                {
                    rel: 'dns-prefetch',
                    href: 'https://platform.twitter.com',
                },
                {
                    // tracking code for Twitter
                    rel: 'dns-prefetch',
                    href: 'https://analytics.twitter.com',
                },
                {
                    // tracking code for google analytics
                    rel: 'dns-prefetch',
                    href: 'https://www.google-analytics.com',
                },
                {
                    // tracking bing
                    rel: 'dns-prefetch',
                    href: 'https://bat.bing.com',
                },
                {
                    // tracking linkedin
                    rel: 'dns-prefetch',
                    href: 'https://snap.licdn.com',
                },
            ],
            script: [
                {
                    // The script itself is included with GTM
                    hid: 'facebook',
                    children: `if (!window.fbq) {
                        window.fbq = (function () {
                            var pendingCalls = [];
            
                            function tmpFbq(command) {
                                if (command === 'getPendingCalls') {
                                    return pendingCalls;
                                }
                                pendingCalls.push(arguments);
                            }
            
                            tmpFbq.isMaltTmpFbq = true;
            
                            return tmpFbq;
                        })();
                    }`,
                    type: 'text/javascript'
                }
            ]
        }
    },

})
