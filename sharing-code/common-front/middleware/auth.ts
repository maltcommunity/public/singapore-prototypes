import {useAuth} from '@/store/auth.store';

export default defineNuxtRouteMiddleware(async (to, from) => {
    const store = useAuth();

    let requiredRoles = Array<string>();

    if (to.meta && to.meta.roles) {
        requiredRoles = to.meta.roles as Array<string>;
    }

    let isLogged = store.isLogged();
    if (!isLogged) {
        return navigateTo('/signin');

    } else {
        if (!store.hasEnoughPrivilege(requiredRoles)) {
            return navigateTo('/');
        }
    }
});
