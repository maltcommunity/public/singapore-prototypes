import i18next from "i18next";
import I18NextVue from 'i18next-vue'
import LanguageDetector from 'i18next-browser-languagedetector';
import ICU from "i18next-icu"
import en from './locales.en.json'
import fr from './locales.fr.json'
import myDetector from '../scripts/i18n-language-detector'


export default defineNuxtPlugin((nuxtApp) => {

    const languageDetector = new LanguageDetector();
    languageDetector.addDetector(myDetector);

    i18next
        .use(languageDetector)
        .use(ICU)
        // init i18next
        // for all options read: https://www.i18next.com/overview/configuration-options
        .init({
            detection : {
                order: ['myDetector'],
            },
            debug: true,
            fallbackLng: 'en',
            resources: {
                en : {
                    translation: en
                },
                fr : {
                    translation: fr
                }
            }
        });

    nuxtApp.vueApp.use(I18NextVue, { i18next })

})
