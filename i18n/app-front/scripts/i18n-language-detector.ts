export default {
    name: 'myDetector',

    lookup(options) {
        const locale = useCookie("i18n").value;

        if (locale) {
            console.log(`Detected locale ${locale}`);
            return locale
        } else {
            console.log(`cookie not present. Using fallback locale ${options.fallBackLng}`);
            return options.fallbackLng
        }
    },

    cacheUserLanguage(lng, options) {
        // options -> are passed in options
        // lng -> current language, will be called after init and on changeLanguage
        // store it
    },
};
