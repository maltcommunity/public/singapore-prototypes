

exports.config = {

    // For every bundle, when the locale is not defined in the file name, the fallback locale is used.
    // example :
    // * admin.properties is equivalent to admin_en.properties
    fallBackLng : 'en',

    // Define the folder where your translations are stored
    basedir: '../common-messages',

    // if you want to filter the keys to import using prefixes
    prefixes : [],

    // target folder
    target : './plugins/'

}
