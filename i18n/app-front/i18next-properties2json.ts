const path = require('path');
const fs = require('fs');
const propertiesReader = require('properties-reader')
const config =  require('./i18nconfig.js').config;

let locales = {};

function readFiles(dirname, locales) {
    fs.readdir(dirname, function(err, filenames) {
        if (err) {
            console.log(err);
            return;
        }

        filenames.forEach(function(filename) {
            console.log('...........................................');

            const pattern = /([a-zA-Z]*)_?([a-z]{2})?.properties/;
            const match = filename.match(pattern);

            const bundle = match[1];
            const locale = match[2] || config.fallBackLng;
            console.log(`Reading ${filename}...`);
            console.log(`bundle = ${bundle}. Locale = ${locale}`);

            const properties = propertiesReader(path.resolve(dirname, filename));

            console.log(properties.length + ' properties found');

            let keys = {};

            properties.each((key, value) => {
                if (config.prefixes && config.prefixes.length > 0) {
                    for (let prefix of config.prefixes) {
                        if (key.startsWith(prefix)) {
                            keys[key] = value;
                            break;
                        }
                    }
                }
                else {
                    keys[key] = value;
                }
            });

            if (locales[locale]) {
                locales[locale] = {...locales[locale], ...keys};
            } else {
                locales[locale] = keys;
            }
        });


        console.log('...........................................');
        console.log("Number of locales supported = " + Object.keys(locales).length);

        Object.keys(locales).forEach(function(locale) {
            // let targetFile =  'locales.'+locale+'.json';
            let targetFile = path.resolve(config.target, 'locales.'+locale+'.json');
            console.log('Writing file ' + targetFile);
            fs.writeFileSync(targetFile, JSON.stringify(locales[locale], null, 2), {encoding: "utf8", flag: "w"});
        });
    });
}

readFiles(config.basedir, locales)



