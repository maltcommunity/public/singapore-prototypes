= Singapore - i18n

How to manage i18n in frontend applications


This project use a synchronization mechanism with common messages thanks to i18next-properties2json plugin

To run it :

    node i18next-properties2json.ts

The behavior can be controlled with the following configuration file :

    i18nconfig.js

