import {resolve} from 'path'
import {defineNuxtModule, extendPages} from '@nuxt/kit'
import {name, version} from '../package.json'

export interface ModuleOptions {
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name,
    version
  },
  setup(options, nuxt) {

    // independent login is only available in dev mode
    if (process.dev) {
      extendPages((pages) => {
        pages.push({
          name: 'Signin',
          path: '/signin',
          file: resolve(__dirname, './runtime/pages/signin.vue')
        })
      })
    }

    nuxt.hook('app:resolve', (app) => {
      app.middleware = app.middleware || []
      app.middleware.push({
        name: 'auth',
        path: resolve('./src/runtime/middleware/auth.ts')
      })

    });

  }
})
