export class User {

    username: string;
    email: string;
    password: string;
    roles: Array<string>
    identities: Array<string>
    selectedIdentity: string
    impersonated: boolean
    impersonatedBy: string

    constructor(username: string, email: string, password: string, roles: Array<string>,
                identities: Array<string>, selectedIdentity: string, impersonated: boolean, impersonatedBy: string) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.identities = identities;
        this.selectedIdentity = selectedIdentity;
        this.impersonated = impersonated;
        this.impersonatedBy = impersonatedBy;
    }

}

