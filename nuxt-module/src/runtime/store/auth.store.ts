import {defineStore} from 'pinia';


export const useAuth = defineStore({
  id: 'auth',

  state: () => {

    const initialState = {status: {loggedIn: false}, user: null };

    return {
      state: initialState,
    };
  },

  actions: {
    login(username: string, password: string) {

      if (username === 'test' && password === 'test') {
        this.state.status.loggedIn = true;
        this.state.user = {username : 'test', roles: ['admin']};
      }

      return this.state.status.loggedIn;
    },
    logout(): void {
      this.state.status.loggedIn = false;
      this.state.user = null;
      useRouter().push('/signin');
    },
    hasEnoughPrivilege(requiredRoles: Array<string>): boolean {
      const noPrivilegeNeeded = requiredRoles.length == 0;

      const hasAllRequiredRoles = requiredRoles.every((role) => this.state.user.roles.includes(role));

      return noPrivilegeNeeded || hasAllRequiredRoles;
    },
    isLogged(): boolean {
      return this.state.status.loggedIn;
    },
  },
});
