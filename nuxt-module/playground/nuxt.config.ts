import { defineNuxtConfig } from 'nuxt/config'
import MyModule from '..'

export default defineNuxtConfig({
  modules: [
    MyModule, "@pinia/nuxt"
  ],
  myModule: {
    addPlugin: true
  }
})
