# use case to reproduce https://github.com/nuxt/framework/issues/7371

## Setup

Make sure to install the dependencies:

```bash
# pnpm
pnpm install 
```

## Development Server

Start the development server on http://localhost:3000/profile

```bash
pnpm run dev
```

