import axios from 'axios';

export default defineNuxtRouteMiddleware(async (to, from) => {

    if (process.client) {
        // @ts-ignore
        if (window.dataLayer) {
            const event = await axios.get('/api/datalayer')
            // @ts-ignore
            window.dataLayer.push(event.data);
        }
    }
})
