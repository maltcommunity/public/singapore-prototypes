
// Fake implementation of the API that returns a datalayer

export default defineEventHandler((event) => {
    return {"name":"NOT_SET",
        "profileId":"NOT_SET",
        "identityIds":["NOT_SET"],
        "admin":"false",
        "loggedUser":"false",
        "visitorCategories":["NOT_SET"]}
})
