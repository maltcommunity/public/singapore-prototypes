export class User {

    token: string;
    username: string;
    email: string;
    password: string;
    roles: Array<string>
    identities: Array<string>
    selectedIdentity: string
    impersonated: boolean
    impersonatedBy: string

    constructor(token: string, username: string, email: string, password: string, roles: Array<string>,
                identities: Array<string>, selectedIdentity: string, impersonated: boolean, impersonatedBy: string) {
        this.token = token;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.identities = identities;
        this.selectedIdentity = selectedIdentity;
        this.impersonated = impersonated;
        this.impersonatedBy = impersonatedBy;
    }

}

