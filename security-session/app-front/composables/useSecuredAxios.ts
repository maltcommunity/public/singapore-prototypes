import axios from 'axios';
import {useAuth} from "~/store/auth.store";


export const useSecuredPostAxios = (url: string, data, options?) => {

    const xsrfCookie = useCookie('XSRF-TOKEN').value;
    return axios.post(url, data,{...options, 'x-xsrf-token': xsrfCookie});
}

export const useSecuredGetAxios = (url: string, options?) => {
    return axios.get(url, options);
}
