import {useAuth} from "@/store/auth.store";

export default defineNuxtRouteMiddleware(async (to, from) => {

    let store = useAuth();

    let requiredRoles = Array<string>();

    if (to.meta && to.meta.roles) {
        requiredRoles = to.meta.roles as Array<string>;
    }

    // Here, we rely on the store to check if the user is logged in and allow them to navigate to the destination page
    // a page will have to include this middleware to protect it from unauthorized access
    let noPrivilegeNeeded = requiredRoles.length == 0;
    let isLogged = await store.isLogged();
    if (!isLogged) {
        return navigateTo('/');
    } else {
        let hasPrivilegeForThisDestination = noPrivilegeNeeded || store.state.user.roles.some(role => requiredRoles.includes(role));
        if (!hasPrivilegeForThisDestination) {
            return navigateTo('/');
        }
    }
})
