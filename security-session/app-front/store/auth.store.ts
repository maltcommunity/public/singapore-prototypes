import {defineStore} from 'pinia'
import axios from "axios";
import {User} from "~/models/User";

const API_URL = 'http://dev.malt.fr/api/auth/';

export const useAuth = defineStore({
    id: 'auth',

    state: () => {

        let user;

        const initialState = {status: {loggedIn: !!user}, user: user as User};

        return {
            state: initialState
        }
    },

    actions: {


        async login(username: string, password: string) {

            const xsrfCookie = useCookie('XSRF-TOKEN').value;

            let response = await useFetch(API_URL + 'signin', {
                method: 'post',
                body: {
                    username: username,
                    password: password
                },
                headers: {
                    'X-XSRF-TOKEN': xsrfCookie
                }
            });

            this.state.status.loggedIn = true;
            this.state.user = response.data;
        },
        logout(): void {
            useSecuredPostAxios(API_URL + 'logout', {}).then(() => {
                this.state.status.loggedIn = false;
                this.state.user = null;
                useRouter().push('/');
            });
        },
        switchIdentity(identity: string): void {
            useSecuredPostAxios(API_URL + `identity/switch/${identity}`, {}).then((response) => {
                this.state.user = response.data;
            });
        },
        impersonate(user: string): void {
            useSecuredPostAxios(API_URL + `sudosu/${user}`, {}).then((response) => {
                this.state.user = response.data;
            });
        },
        async isLogged(): Promise<boolean> {

            // If the user is not logged, we ask the server. In this situation, it's possible the user refreshed the page
            // but the session is still valid.
            // this situation only happens on secured page, if the user is not logged
            if (!this.state.status.loggedIn) {
                try {
                    let config = {};
                    if (process.server) {
                        let session = useCookie('SESSION').value;
                        config = {headers : {Cookie : 'SESSION='+ session}};
                    }
                    let response = await axios.get(API_URL + 'me',  config);
                    this.state.user = response.data;
                    this.state.status.loggedIn = true;
                } catch (error) {
                    console.log('No user found ' + error);
                }
            }
            return this.state.status.loggedIn;
        }
    },
});
