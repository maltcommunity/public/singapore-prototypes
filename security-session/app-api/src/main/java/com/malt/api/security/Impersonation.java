package com.malt.api.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
public class Impersonation implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean impersonated;
    private String impersonatedBy;
}
