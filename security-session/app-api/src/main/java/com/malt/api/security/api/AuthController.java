package com.malt.api.security.api;

import com.malt.api.security.Impersonation;
import com.malt.api.security.UserDetailsImpl;
import com.malt.api.security.UserDetailsServiceImpl;
import com.malt.api.security.payload.request.LoginRequest;
import com.malt.api.security.payload.response.LoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @GetMapping("/me")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<LoginResponse> getCurrentUser() {
        UserDetailsImpl principal = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        LoginResponse loginResponse = createLoginResponse(principal);

        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("/sudosu/{user}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity sudosu(@PathVariable String user) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        // the token is validated, and the user is an admin
        // we load the user to impersonate from the database and we change the current session to impersonate the user
        // however, we have to keep the information of the current admin to be able to restore the impersonation
        String initialUser = userDetails.getUsername();

        UserDetailsImpl userDetailsFromDb = (UserDetailsImpl) userDetailsService.loadUserByUsername(user);
        Impersonation impersonation = new Impersonation();
        impersonation.setImpersonated(true);
        impersonation.setImpersonatedBy(initialUser);
        userDetailsFromDb.setImpersonation(impersonation);

        SecurityContextHolder.getContext().setAuthentication(null);
        SecurityContextHolder.clearContext();

        UsernamePasswordAuthenticationToken authenticated =
                UsernamePasswordAuthenticationToken.authenticated(userDetailsFromDb, userDetailsFromDb.getPassword(), userDetailsFromDb.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticated);

        LoginResponse loginResponse = createLoginResponse(userDetailsFromDb);

        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("/identity/switch/{identity}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity switchIdentity(@PathVariable String identity) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        userDetails.setSelectedIdentity(identity);

        SecurityContextHolder.getContext().setAuthentication(null);
        SecurityContextHolder.clearContext();

        SecurityContextHolder.getContext().setAuthentication(authentication);

        LoginResponse loginResponse = createLoginResponse(userDetails);

        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("/signin")
    public ResponseEntity<LoginResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        LoginResponse loginResponse = createLoginResponse(userDetails);

        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("/logout")
    public ResponseEntity logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        SecurityContextHolder.clearContext();
        return ResponseEntity.ok().build();
    }


    private LoginResponse createLoginResponse(UserDetailsImpl userDetails) {

        LoginResponse loginResponse = new LoginResponse(
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getRoles(),
                userDetails.getIdentities(),
                userDetails.getSelectedIdentity());

        if (userDetails.getImpersonation() != null) {
            loginResponse.setImpersonated(userDetails.getImpersonation().isImpersonated());
            loginResponse.setImpersonatedBy(userDetails.getImpersonation().getImpersonatedBy());
        }

        return loginResponse;
    }
}
