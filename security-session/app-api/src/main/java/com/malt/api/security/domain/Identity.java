package com.malt.api.security.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Identity {

    private String id;

}
