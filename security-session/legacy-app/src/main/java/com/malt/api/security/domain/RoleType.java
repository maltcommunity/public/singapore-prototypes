package com.malt.api.security.domain;

public enum RoleType {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN

}
