package com.malt.api.security.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Identity {

    private String id;

}
