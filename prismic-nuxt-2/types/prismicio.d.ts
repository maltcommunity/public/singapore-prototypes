declare module '@prismicio/vue/components' {
  import { SliceComponentProps } from '~/types/prismicTypes'

  function getSliceComponentProps<
    P extends Record<string, AnyRegularField>,
    I extends Record<string, AnyRegularField>
  >(): SliceComponentProps<P, I>
}
