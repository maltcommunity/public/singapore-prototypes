import {
  AnyRegularField,
  SharedSlice,
  SharedSliceVariation,
} from '@prismicio/types'
import { PropValidator } from 'vue/types/options'

export type SharedSliceAttributes<
  P extends Record<string, AnyRegularField>,
  I extends Record<string, AnyRegularField>
> = {
  index: number
  context: null
  slice: SharedSlice<string, SharedSliceVariation<string, P, I>>
  slices: SharedSlice<string, SharedSliceVariation<string, P, I>>[]
}

export type PrismicComputed<
  P extends Record<string, AnyRegularField>,
  I extends Record<string, AnyRegularField>,
  CustomComputed
> = SharedSliceAttributes<P, I> & CustomComputed

export type SliceComponentProps<
  P extends Record<string, AnyRegularField>,
  I extends Record<string, AnyRegularField>,
  CustomComputed,
  Refs,
  Props = {}
> = {
  slice: SharedSlice<string, SharedSliceVariation<string, P, I>> & {
    id: string
  }
  index: PropValidator<number>
  context: null
  slices: SharedSlice<string, SharedSliceVariation<string, P, I>>[]
  $data: CustomComputed
  $refs: Refs
} & Props
