package com.malt.api;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Accessors(chain = true)
public class HostSettings {

  private String id;

  @NotBlank
  private String url;

  private String email;

  private List<String> socialNetworks ;

  @NotBlank
  private TypeEnum type;

}

