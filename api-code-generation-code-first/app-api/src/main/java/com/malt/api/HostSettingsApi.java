package com.malt.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600, origins = "*")
@RequestMapping("/api/hostsettings")
public class HostSettingsApi {

    @GetMapping
    public ResponseEntity<HostSettings> getApiHostsettings(@RequestParam String id) {
        return ResponseEntity.ok(
                new HostSettings()
                        .setId(id)
                        .setUrl("http://localhost:8080")
                        .setEmail("    ")
                        .setType(TypeEnum.BETA)
                        .setSocialNetworks(List.of("facebook"))
        );
    }

    @GetMapping("/all")
    public List<HostSettings> getAllHostsettings() {

        return List.of(
                new HostSettings()
                        .setId("1")
                        .setUrl("http://localhost:8080")
                        .setEmail("    ")
                        .setType(TypeEnum.BETA)
                        .setSocialNetworks(List.of("facebook")),
                new HostSettings()
                        .setId("2")
                        .setUrl("http://localhost:8080")
                        .setEmail("    ")
                        .setType(TypeEnum.BETA)
                        .setSocialNetworks(List.of("facebook"))
        );
    }

    @PostMapping
    public HostSettings addHostSettings(@RequestBody HostSettings hostSettings) {
        return hostSettings;
    }

}
