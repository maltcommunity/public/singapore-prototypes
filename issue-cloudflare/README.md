# Reproduce failed build with cloudflare preset


## Setup

Make sure to install the dependencies:

```bash
# pnpm
pnpm install 
```

Build the application 
```bash
# pnpm
pnpm run build
```
