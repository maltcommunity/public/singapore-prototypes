// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@nuxtjs/prismic'],
  prismic: { endpoint: 'https://malt-cms-marketing-dev.cdn.prismic.io/api/v2' },
});
