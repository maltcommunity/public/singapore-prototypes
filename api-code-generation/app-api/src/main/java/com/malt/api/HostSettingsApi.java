package com.malt.api;

import com.malt.api.model.HostSettings;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class HostSettingsApi implements ApiApiDelegate {

    @Override
    public ResponseEntity<HostSettings> getApiHostsettings() {
        return ResponseEntity.ok(
                new HostSettings()
                .url("http://localhost:8080")
                .email("    ").addSocialNetworksItem("facebook")
                .type(HostSettings.TypeEnum.BETA)
                .addSocialNetworksItem("twitter")
        );
    }

}
